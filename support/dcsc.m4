dnl -*- mode: Autoconf -*- 
dnl
dnl  ROOT generic dcsc framework 
dnl  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
dnl
dnl  This library is free software; you can redistribute it and/or 
dnl  modify it under the terms of the GNU Lesser General Public License 
dnl  as published by the Free Software Foundation; either version 2.1 
dnl  of the License, or (at your option) any later version. 
dnl
dnl  This library is distributed in the hope that it will be useful, 
dnl  but WITHOUT ANY WARRANTY; without even the implied warranty of 
dnl  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
dnl  Lesser General Public License for more details. 
dnl 
dnl  You should have received a copy of the GNU Lesser General Public 
dnl  License along with this library; if not, write to the Free 
dnl  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 
dnl  02111-1307 USA 
dnl
dnl
dnl __________________________________________________________________
dnl
dnl AC_DCSC([MINIMUM-VERSION 
dnl             [,ACTION-IF_FOUND 
dnl              [, ACTION-IF-NOT-FOUND]])
AC_DEFUN([AC_DCSC],
[
    # Command line argument to specify prefix. 
    AC_ARG_WITH([dcsc],
        [AC_HELP_STRING([--with-dcsc],	
                        [Prefix where Dcsc is installed])],
	[],[with_dcsc="yes"])

    # Command line argument to specify documentation URL. 
    AC_ARG_WITH([dcsc-url],
        [AC_HELP_STRING([--with-dcsc-url],
		[Base URL where the Dcsc dodumentation is installed])],
        dcsc_url=$withval, dcsc_url="")
    if test "x${DCSC_CONFIG+set}" != xset ; then 
        if test "x$with_dcsc" != "xno" ; then 
	    DCSC_CONFIG=$with_dcsc/bin/dcsc-config
	fi
    fi   
	
    # Check for the configuration script. 
    if test "x$with_dcsc" != "xno" ; then 
        AC_PATH_PROG(DCSC_CONFIG, dcsc-config, no)
        dcsc_min_version=ifelse([$1], ,0.3,$1)
        # Message to user
        AC_MSG_CHECKING(for Dcsc version >= $dcsc_min_version)

        # Check if we got the script
        with_dcsc=no    
        if test "x$DCSC_CONFIG" != "xno" ; then 
           # If we found the script, set some variables 
           DCSC_CPPFLAGS=`$DCSC_CONFIG --cppflags`
           DCSC_INCLUDEDIR=`$DCSC_CONFIG --includedir`
           DCSC_LIBS=`$DCSC_CONFIG --libs`
           DCSC_LTLIBS=`$DCSC_CONFIG --ltlibs`
           DCSC_LIBDIR=`$DCSC_CONFIG --libdir`
           DCSC_LDFLAGS=`$DCSC_CONFIG --ldflags`
           DCSC_LTLDFLAGS=`$DCSC_CONFIG --ltldflags`
           DCSC_PREFIX=`$DCSC_CONFIG --prefix`
           
           # Check the version number is OK.
           dcsc_version=`$DCSC_CONFIG -V` 
           dcsc_vers=`echo $dcsc_version | \
             awk 'BEGIN { FS = "."; } \
    	   { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
           dcsc_regu=`echo $dcsc_min_version | \
             awk 'BEGIN { FS = "."; } \
    	   { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
           if test $dcsc_vers -ge $dcsc_regu ; then 
                with_dcsc=yes
           fi
        fi
        AC_MSG_RESULT($with_dcsc - is $dcsc_version) 
    
        # Some autoheader templates. 
        AH_TEMPLATE(HAVE_DCSC, [Whether we have dcsc])
    
    
        if test "x$with_dcsc" = "xyes" ; then
            # Now do a check whether we can use the found code. 
            save_LDFLAGS=$LDFLAGS
    	    save_CPPFLAGS=$CPPFLAGS
	    save_LIBS=$LIBS
            LDFLAGS="$LDFLAGS $DCSC_LDFLAGS"
            CPPFLAGS="$CPPFLAGS $DCSC_CPPFLAGS"
     
     	    # Check for a header 
            have_dcsc_dcscMsgBufferInterface_h=0
            AC_CHECK_HEADER([dcsc/dcscMsgBufferInterface.h], 
                            [have_dcsc_dcscMsgBufferInterface_h=1])
    
            # Check the library. 
            have_libdcsc=no
            AC_MSG_CHECKING(for -ldcsc)
            AC_CHECK_LIB([dcsc],[initRcuAccess],[have_libdcsc=yes])
            AC_MSG_RESULT($have_libdcsc)
    
            if test $have_dcsc_dcscMsgBufferInterface_h -gt 0    && \
                test "x$have_libdcsc"   = "xyes" ; then
    
                # Define some macros
                AC_DEFINE(HAVE_DCSC)
            else 
                with_dcsc=no
            fi
    	    CPPFLAGS=$save_CPPFLAGS
    	    LDFLAGS=$save_LDFLAGS
	    LIBS=$save_LIBS
        fi
    
        AC_MSG_CHECKING(where the Dcsc documentation is installed)
        if test "x$dcsc_url" = "x" && \
    	test ! "x$DCSC_PREFIX" = "x" ; then 
           DCSC_URL=${DCSC_PREFIX}/share/doc/dcsc
        else 
    	   DCSC_URL=$dcsc_url
        fi	
        AC_MSG_RESULT($DCSC_URL)
    fi
   
    if test "x$with_dcsc" = "xyes" ; then 
        ifelse([$2], , :, [$2])
    else 
        ifelse([$3], , :, [$3])
    fi
    AC_SUBST(DCSC_URL)
    AC_SUBST(DCSC_PREFIX)
    AC_SUBST(DCSC_CPPFLAGS)
    AC_SUBST(DCSC_INCLUDEDIR)
    AC_SUBST(DCSC_LDFLAGS)
    AC_SUBST(DCSC_LIBDIR)
    AC_SUBST(DCSC_LIBS)
    AC_SUBST(DCSC_LTLIBS)
    AC_SUBST(DCSC_LTLDFLAGS)
])
dnl
dnl EOF
dnl 
