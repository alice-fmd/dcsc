dnl
dnl
dnl  Copyright (C) 2002 Christian Holm Christensen <cholm@nbi.dk>
dnl
dnl  This library is free software; you can redistribute it and/or
dnl  modify it under the terms of the GNU Lesser General Public License
dnl  as published by the Free Software Foundation; either version 2.1
dnl  of the License, or (at your option) any later version.
dnl
dnl  This library is distributed in the hope that it will be useful,
dnl  but WITHOUT ANY WARRANTY; without even the implied warranty of
dnl  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
dnl  Lesser General Public License for more details.
dnl
dnl  You should have received a copy of the GNU Lesser General Public
dnl  License along with this library; if not, write to the Free
dnl  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
dnl  02111-1307 USA
dnl
dnl ------------------------------------------------------------------
AC_DEFUN([AC_THREAD_FLAGS],
[
  dnl ------------------------------------------------------------------
  dnl Thread flags to use 
  case $host_os:$host_cpu in
  solaris*|sun*)	  CFLAGS="$CFLAGS -mt"
			  LIBS="$LIBS -lposix4"		;;
  hp-ux*|osf*|aix*)					;;
  # The DIM libary should not be threaded on an ARM chip, but the
  # Dcsc  assumes that it is so we take the next line out and
  # default to normal Linux
  # linux*:arm*)					;;
  linux*)		  LIBS="$LIBS -pthread"		;;
  lynxos*:rs6000)	  CFLAGS="$CFLAGS -mthreads"	;;
  *)			  LIBS="$LIBS -lpthread"	;;
  esac
])

AC_DEFUN([AC_DIM_ARCH],
[
  AC_REQUIRE([AC_PROG_CC])
  dnl ------------------------------------------------------------------
  dnl Byte order 
  AH_TEMPLATE(MIPSEB, [Big-endian machine])
  AH_TEMPLATE(MIPSEL, [Little-endian machine])
  AC_C_BIGENDIAN([AC_DEFINE([MIPSEB])],[AC_DEFINE([MIPSEL])])
  AC_DEFINE([PROTOCOL],[1])

  AC_REQUIRE([AC_THREAD_FLAGS])
  dnl ------------------------------------------------------------------
  dnl Misc flags per host OS/CPU
  case $host_os:$host_cpu in
  sun*)		  AC_DEFINE([sunos])		;;
  solaris*)	  AC_DEFINE([solaris])	
		  LIBS="$LIBS -lsocket -lnsl"	;;
  hp-ux*)	  AC_DEFINE([hpux])		;;
  osf*)		  AC_DEFINE([osf])		;;
  aix*)		  AC_DEFINE([aix])	
		  AC_DEFINE([unix])
		  AC_DEFINE([_BSD])		;;
  lynxos*:rs6000) AC_DEFINE([LYNXOS])	
		  AC_DEFINE([RAID])
		  AC_DEFINE([unix])		
		  CPPFLAGS="$CPPFLAGS -I/usr/include/bsd -I/usr/include/posix"
		  LDFLAGS="$LDFLAGS -L/usr/posix/usr/lib"
		  LIBS="$LIBS -lbsd"		;;
  lynxos*:*86*)	  AC_DEFINE([LYNXOS])	
		  AC_DEFINE([unix])		
		  LIBS="$LIBS -lbsd -llynx"	;;
  lynxos*)	  AC_DEFINE([LYNXOS])	
		  AC_DEFINE([unix])		
		  LIBS="$LIBS -lbsd"		;;
  linux*)	  AC_DEFINE([linux])		
		  AC_DEFINE([unix])		;;
  esac
])

dnl ------------------------------------------------------------------
AC_DEFUN([AC_DIM],
[
  AC_REQUIRE([AC_DIM_ARCH])
  dnl ------------------------------------------------------------------
  AC_ARG_ENABLE([builtin-dim],
	        [AC_HELP_STRING([--enable-builtin-dim],
	                        [Use shipped DIM, not systems])])
  have_dim=no
  AC_LANG_PUSH(C++)
  AC_CHECK_LIB(dim, [dic_get_server],
	       [AC_CHECK_HEADERS([dim/dim.hxx],[have_dim=yes])])
  AC_LANG_POP(C++)
  if test "x$enable_builtin_dim" = "xyes" ; then 
     have_dim=no
  fi
  AC_MSG_CHECKING(whether to use possible system DIM installed)
  if test "x$have_dim" = "xno" ; then 
     AC_CONFIG_SUBDIRS(dim)
  fi
  AC_MSG_RESULT($have_dim)
  AM_CONDITIONAL(NEED_DIM, test "x$have_dim" = "xno")
])

dnl ------------------------------------------------------------------
AC_DEFUN([AC_DEBUG],
[
  AC_REQUIRE([AC_PROG_CC])
  AC_MSG_CHECKING(whether to make debug objects)
  AC_ARG_ENABLE(debug,
    [AC_HELP_STRING([--enable-debug],[Enable debugging symbols in objects])],
    [],[enable_debug=yes])
  if test "x$enable_debug" = "xno" ; then
    CFLAGS=`echo $CFLAGS | sed 's,-g,,'`
  else
    AC_DEFINE(__DEBUG)
    case $CFLAGS in
    *-g*) ;;
    *)    CFLAGS="$CFLAGS -g" ;;
    esac
  fi
  AC_MSG_RESULT($enable_debug 'CFLAGS=$CFLAGS')
])

dnl ------------------------------------------------------------------
AC_DEFUN([AC_OPTIMIZATION],
[
  AC_REQUIRE([AC_PROG_CC])
  AC_ARG_ENABLE(optimization,
    [AC_HELP_STRING([--enable-optimization],[Enable optimization of objects])],
    [],[enable_optimization=yes])

  AC_MSG_CHECKING(for optimiztion level)

  changequote(<<, >>)dnl
  if test "x$enable_optimization" = "xno" ; then
    CFLAGS=`echo   $CFLAGS   | sed 's,-O\([0-9][0-9]*\|\),,'`
  elif test "x$enable_optimization" = "xyes" ; then
    case $CFLAGS in
    *-O*) ;;
    *)    CFLAGS="$CXXFLAGS -O2" ;;
    esac
  else
    CFLAGS=`echo $CFLAGS | sed "s,-O\([0-9][0-9]*\|\),-O$enable_optimization,"`
  fi
  changequote([, ])dnl
  AC_MSG_RESULT($enable_optimization 'CFLAGS=$CFLAGS')
])

#
# EOF
#
